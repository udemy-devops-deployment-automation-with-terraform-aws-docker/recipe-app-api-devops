#!/bin/sh

set -e

# Django command to collect all static files for a project and store them in 1 dir
python manage.py collectstatic --noinput

# wait for the database to be available so that we can run our migrations
python manage.py wait_for_db

python manage.py migrate

uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi
