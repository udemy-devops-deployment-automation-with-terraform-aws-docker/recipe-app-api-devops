variable "prefix" {
  default = "raad" # Recipe App API Devops
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "telluur@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}